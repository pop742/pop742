<!--
 * @Author       : ZZZYD72 ZZZYD72@outlook.com
 * @Date         : 2023-01-18 09:00:18
 * @LastEditors  : ZZZYD72 ZZZYD72@outlook.com
 * @LastEditTime : 2023-01-18 09:21:04
 * @FilePath     : /pop742/text/environment-make/index.cn.md
 * @LICENSE      : MIT
 * @*            :
-->
# 环境配置

- ```Brew```
- ```Xcode Tool```
- ```JetBrain Mono```
- ```Git```
- ```VS Code```
- ```Zsh```
- ```StarShip```
